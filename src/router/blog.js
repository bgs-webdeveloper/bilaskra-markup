export default [
    {
        path: '/blog',
        name: 'Blog',
        component: () => import('../pages/Blog/Index.vue')
    },
    {
        path: '/frettir',
        name: 'Fréttir',
        component: () => import('../pages/Blog/Frettir.vue')
    },
    {
        path: '/frettir/:id',
        name: 'FréttirSimplePost',
        component: () => import('../pages/Blog/SimplePost.vue')
    },
    {
        path: '/vidburdir',
        name: 'Viðburðir',
        component: () => import('../pages/Blog/Vidburdir.vue')
    },
    {
        path: '/vidburdir/:id',
        name: 'ViðburðirSimplePost',
        component: () => import('../pages/Blog/SimplePost.vue')
    },
    {
        path: '/reynsluakstur',
        name: 'Reynsluakstur',
        component: () => import('../pages/Blog/Reynsluakstur.vue')
    },
    {
        path: '/reynsluakstur/:id',
        name: 'ReynsluaksturSimplePost',
        component: () => import('../pages/Blog/SimplePost.vue')
    }
]
