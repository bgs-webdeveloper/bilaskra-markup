import Vue from 'vue'
import Router from 'vue-router'

import blog from './blog'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('../pages/Blog/Index.vue')
    },
    ...blog
  ]
})
