export const blogPage = {
  filtersNames: ['JEPPAR Á ÍSLANDI', 'RAFBÍLAR', 'MYNDBAND DAGSINS'],
  posts: [
    {
      id: 0,
      image: 'https://photos.clutch.ca/af84be20-558e-11e9-8ddd-e389f2c9913a-medium.jpg',
      title: 'Title',
      category: 'FRÉTTIR',
      model: 'MERCEDES-BENZ',
      date: '1. JÚNÍ 2019'
    },
    {
      id: 1,
      image: 'https://photos.clutch.ca/af84be20-558e-11e9-8ddd-e389f2c9913a-medium.jpg',
      title: 'Title',
      category: 'FRÉTTIR',
      model: 'MERCEDES-BENZ',
      date: '1. JÚNÍ 2019',
      filter: 'JEPPAR Á ÍSLANDI'
    },
    {
      id: 2,
      image: 'https://photos.clutch.ca/af84be20-558e-11e9-8ddd-e389f2c9913a-medium.jpg',
      title: 'GLC í uppfærðri útgáfu',
      category: 'FRÉTTIR',
      model: 'MERCEDES-BENZ',
      date: '1. JÚNÍ 2019',
      filter: 'RAFBÍLAR'
    },
    {
      id: 3,
      image: 'https://photos.clutch.ca/af84be20-558e-11e9-8ddd-e389f2c9913a-medium.jpg',
      title: 'Title',
      category: 'FRÉTTIR',
      model: 'MERCEDES-BENZ',
      date: '1. JÚNÍ 2019',
      filter: 'MYNDBAND DAGSINS'
    },
    {
      id: 4,
      image: 'https://photos.clutch.ca/af84be20-558e-11e9-8ddd-e389f2c9913a-medium.jpg',
      title: 'Title',
      category: 'FRÉTTIR',
      model: 'MERCEDES-BENZ',
      date: '1. JÚNÍ 2019',
      filter: 'RAFBÍLAR'
    }
  ]
}

export const simplePage = {
  /* common: {
    id: 0,
    slug: 'slug-page',
    description: '',
    author: {
      id: 0,
      img: {
        url: 'https://photos.clutch.ca/af84be20-558e-11e9-8ddd-e389f2c9913a-medium.jpg',
        title: 'title',
        alt: 'alt'
      }
    },
    model: 'Mercedes Benz B Class',
    wishes: 'Óskar Bílakall',
    category: 'Reynsluakstur',
    date: '30. MAÍ 2019'
  },
  sections: [
    {
      id: 0,
      type: 'banner',
      title: 'Stjarna fyrir alla fjölskylduna',
      img: {
        url: 'https://photos.clutch.ca/af84be20-558e-11e9-8ddd-e389f2c9913a-medium.jpg',
        title: 'title',
        alt: 'alt'
      }
    },
    {
      id: 1,
      type: 'info',
      description: 'Ef þig vantar bíl sem svarar kalli fjölskyldunnar um pláss, áreiðanleika, fallega hönnun, gæða samsetningu og frábæra tækni þá er Mercedes Bens B-Class eitthvað fyrir þig.',
      description2: '',
      table: [
        {
          id: 0,
          type: 'Mercedes Benz B Class',
          year: '2019',
          transmission: 'Askja',
          link: '/'
        },
        {
          id: 1,
          type: 'Mercedes Benz B Class',
          year: '2019',
          transmission: 'Askja',
          link: '/'
        }
      ]
    },
    {
      id: 2,
      type: 'slider',
      slides: [
        {
          id: 0,
          img: {
            url: 'https://photos.clutch.ca/af84be20-558e-11e9-8ddd-e389f2c9913a-medium.jpg',
            title: 'title',
            alt: 'alt'
          }
        },
        {
          id: 1,
          img: {
            url: 'https://photos.clutch.ca/af84be20-558e-11e9-8ddd-e389f2c9913a-medium.jpg',
            title: 'title',
            alt: 'alt'
          }
        }
      ]
    }
  ], */

  //

  id: 0,
  banner: {
    img: {
      url: 'https://photos.clutch.ca/af84be20-558e-11e9-8ddd-e389f2c9913a-medium.jpg',
      title: 'title',
      alt: 'alt'
    },
    title: 'Title',
    breadcrubms: [
      {
        title: 'Óskar Bílakall',
        link: '/'
      },
      {
        title: 'Reynsluakstur',
        link: '/'
      },
      {
        title: '30. MAÍ 2019',
        link: '/'
      }
    ]
  },

  info: {
    text: 'text',
    table: [
      {
        columns: ['Mercedes Benz B Class dfghjghj', '2019', 'Askja'],
        link: '/'
      },
      {
        columns: ['Mercedes Benz B Class', '2019', 'Askja'],
        link: '/fghjmgh'
      }
    ]
  },

  id: 0,
  bannerImgUrl: 'https://photos.clutch.ca/af84be20-558e-11e9-8ddd-e389f2c9913a-medium.jpg',
  bannerAvatarUrl: 'https://media.istockphoto.com/photos/businessman-silhouette-as-avatar-or-default-profile-picture-picture-id476085198?k=6&m=476085198&s=612x612&w=0&h=5cDQxXHFzgyz8qYeBQu2gCZq1_TN0z40e_8ayzne0X0=',
  bannerTitle: 'Stjarna fyrir alla fjölskylduna',
  bannerBreadcrumbs: [
    {
      title: 'Óskar Bílakall',
      link: '/'
    },
    {
      title: 'Reynsluakstur',
      link: '/'
    },
    {
      title: '30. MAÍ 2019',
      link: '/'
    }
  ],
  infoDescription: 'Ef þig vantar bíl sem svarar kalli fjölskyldunnar um pláss, áreiðanleika, fallega hönnun, gæða samsetningu og frábæra tækni þá er Mercedes Bens B-Class eitthvað fyrir þig.',
  infoTable: [
    {
      columns: ['Mercedes Benz B Class dfghjghj', '2019', 'Askja'],
      link: '/'
    },
    {
      columns: ['Mercedes Benz B Class', '2019', 'Askja'],
      link: '/fghjmgh'
    }
  ],
  infoImgUrl: 'https://photos.clutch.ca/af84be20-558e-11e9-8ddd-e389f2c9913a-medium.jpg',
  infoImgText: 'Ljósahönnun B-Class er vel heppnuð eins og allur bílinn. Fullbúinn með LED ljósum bæði að framan og aftan.',
  infoDescription2: 'Við Íslendingar erum án efa dugleg að kaupa okkur jepplinga fyrir fjölskylduna okkar. En hvað ef þig langar ekki í svoleiðis? Hvað ef þig langar í fólksbíl sem býður uppá nægt pláss fyrir alla fjölskylduna og hundinn líka? Þá er Mercedes Bens B-Class málið. Fjölnota fjölskyldubíll sem mætir á svæðið pakkaður snjöllum lausnum fyrir fjölskyldur.',

  sliderImages: [
    'https://photos.clutch.ca/af84be20-558e-11e9-8ddd-e389f2c9913a-medium.jpg',
    'https://s.aolcdn.com/dims-global/dims3/GLOB/legacy_thumbnail/350x197/quality/95/https://s.blogcdn.com/slideshows/images/slides/732/758/3/S7327583/slug/l/01-bugatti-chiron-sport-geneva-1-2.jpg',
    'https://s.aolcdn.com/dims-global/dims3/GLOB/legacy_thumbnail/350x197/quality/95/https://s.blogcdn.com/slideshows/images/slides/732/758/3/S7327583/slug/l/01-bugatti-chiron-sport-geneva-1-2.jpg',
    'https://s.aolcdn.com/dims-global/dims3/GLOB/legacy_thumbnail/350x197/quality/95/https://s.blogcdn.com/slideshows/images/slides/732/758/3/S7327583/slug/l/01-bugatti-chiron-sport-geneva-1-2.jpg',
    'https://s.aolcdn.com/dims-global/dims3/GLOB/legacy_thumbnail/350x197/quality/95/https://s.blogcdn.com/slideshows/images/slides/732/758/3/S7327583/slug/l/01-bugatti-chiron-sport-geneva-1-2.jpg',
    'https://s.aolcdn.com/dims-global/dims3/GLOB/legacy_thumbnail/350x197/quality/95/https://s.blogcdn.com/slideshows/images/slides/732/758/3/S7327583/slug/l/01-bugatti-chiron-sport-geneva-1-2.jpg',
    'https://s.aolcdn.com/dims-global/dims3/GLOB/legacy_thumbnail/350x197/quality/95/https://s.blogcdn.com/slideshows/images/slides/732/758/3/S7327583/slug/l/01-bugatti-chiron-sport-geneva-1-2.jpg'
  ],

  salonText1: 'Þegar jafn öflugur bíll og Mercedes B-Class er í boði fyrir fjölskyldur landsins er erfitt að átta sig á af hverju fólk myndi frekar fá sér lítinn jeppling með jafnvel minna plássi. B-Class mætir á svæðið vel útbúinn og býður uppá frábæra aksturseiginleika með feikinógu plássi fyrir alla. Hann er þægilegur í umgengi og fallegur í útliti. Ég mæli með B-Class fyrir alla þá sem vilja þægilegan og skemmtilegan bíl sem býður uppá mikið af möguleikum til að gera hann persónulegan. Ég mæli með honum í rauðu eins og prufubílinn, og í progressive útfærslu. Ef þú vilt svo sérpanta þér bíl þá mæli ég alveg sérstaklega með 190 hestafla fjórhjóladrifsútfærslunni.',
  salonText2: 'Þegar jafn öflugur bíll og Mercedes B-Class er í boði fyrir fjölskyldur landsins er erfitt að átta sig á af hverju fólk myndi frekar fá sér lítinn jeppling með jafnvel minna plássi. B-Class mætir á svæðið vel útbúinn og býður uppá frábæra aksturseiginleika með feikinógu plássi fyrir alla. Hann er þægilegur í umgengi og fallegur í útliti. Ég mæli með B-Class fyrir alla þá sem vilja þægilegan og skemmtilegan bíl sem býður uppá mikið af möguleikum til að gera hann persónulegan. Ég mæli með honum í rauðu eins og prufubílinn, og í progressive útfærslu. Ef þú vilt svo sérpanta þér bíl þá mæli ég alveg sérstaklega með 190 hestafla fjórhjóladrifsútfærslunni.',
  salonImage1: 'https://s.aolcdn.com/dims-global/dims3/GLOB/legacy_thumbnail/350x197/quality/95/https://s.blogcdn.com/slideshows/images/slides/732/758/3/S7327583/slug/l/01-bugatti-chiron-sport-geneva-1-2.jpg',
  salonImage2: 'https://s.aolcdn.com/dims-global/dims3/GLOB/legacy_thumbnail/350x197/quality/95/https://s.blogcdn.com/slideshows/images/slides/732/758/3/S7327583/slug/l/01-bugatti-chiron-sport-geneva-1-2.jpg',

  conclusionText: 'Þegar jafn öflugur bíll og Mercedes B-Class er í boði fyrir fjölskyldur landsins er erfitt að átta sig á af hverju fólk myndi frekar fá sér lítinn jeppling með jafnvel minna plássi. B-Class mætir á svæðið vel útbúinn og býður uppá frábæra aksturseiginleika með feikinógu plássi fyrir alla. Hann er þægilegur í umgengi og fallegur í útliti. Ég mæli með B-Class fyrir alla þá sem vilja þægilegan og skemmtilegan bíl sem býður uppá mikið af möguleikum til að gera hann persónulegan. Ég mæli með honum í rauðu eins og prufubílinn, og í progressive útfærslu. Ef þú vilt svo sérpanta þér bíl þá mæli ég alveg sérstaklega með 190 hestafla fjórhjóladrifsútfærslunni.',
  conclusionPrice: '5.230.000',
  conclusionBtnLinkFacebook: '#',
  conclusionBtnLink: '#',

  similarTitle: 'Aðrar umfjallanir',
  similarCars: [
    {
      image: 'https://s.aolcdn.com/dims-global/dims3/GLOB/legacy_thumbnail/350x197/quality/95/https://s.blogcdn.com/slideshows/images/slides/732/758/3/S7327583/slug/l/01-bugatti-chiron-sport-geneva-1-2.jpg',
      title: 'Rafmagnaður til að vera betri',
      model: 'Audi Q7 E-Tron'
    },
    {
      image: 'https://s.aolcdn.com/dims-global/dims3/GLOB/legacy_thumbnail/350x197/quality/95/https://s.blogcdn.com/slideshows/images/slides/732/758/3/S7327583/slug/l/01-bugatti-chiron-sport-geneva-1-2.jpg',
      title: '',
      model: 'Audi Q7 E-Tron'
    },
    {
      image: 'https://s.aolcdn.com/dims-global/dims3/GLOB/legacy_thumbnail/350x197/quality/95/https://s.blogcdn.com/slideshows/images/slides/732/758/3/S7327583/slug/l/01-bugatti-chiron-sport-geneva-1-2.jpg',
      title: '',
      model: 'Audi Q7 E-Tron'
    }
  ]
}
