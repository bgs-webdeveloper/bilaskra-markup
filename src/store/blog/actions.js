import { blogPage, simplePage } from '../../temporary_api/blog'
import Axios from 'axios'

export default {
  SET_DATA_BLOG_PAGE (context) {
    const data = {
      field: 'blogPage',
      data: blogPage
    }
    context.commit('SET_DATA', data)
  },
  SET_DATA_SIMPLE_PAGE (context) {
    const data = {
      field: 'simplePage',
      data: simplePage
    }
    context.commit('SET_DATA', data)
  },
  GET_DATA_POST (context, payload) {
    Axios.get(`${context.state.api.admin}blog/${payload}`)
      .then((res) => {
        const data = {
          field: 'simplePage',
          data: res.data
        }
        context.commit('SET_DATA', data)
      })
      .catch((err) => console.log(err))
  },
  GET_SIMILAR_POSTS (context, payload) {

  }
}
