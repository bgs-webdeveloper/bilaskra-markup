export default {
  // Data in News page
  GET_DATA_BLOG_PAGE: state => {
    const data = state.blogPage
    return {
      filters: data.filtersNames,
      posts: data.posts
    }
  },

  // Data in Test Drive Simple car page
  GET_SECTIONS_SIMPLE_PAGE: state => {
    const data = state.simplePage
    return data && data.sections
  },

  GET_MAIN_DATA_SIMPLE_PAGE: state => {
    const data = state.simplePage
    let category = ''
    let categoryUrlName = ''
    state.category.forEach((item) => {
      if (item.id === data.category_id) {
        category = item.title
        categoryUrlName = item.url
      }
    })
    return {
      author: data.author,
      category: category,
      categoryUrlName: categoryUrlName
    }
  },

  GET_SIMILAR_POSTS: state => {
    const posts = state.posts
    return posts
  }
}
