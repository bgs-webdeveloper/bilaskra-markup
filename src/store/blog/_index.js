import actions from './actions'
import getters from './getters'

const host = 'https://dev.bilaskra.is' /* 'https://dev2.arthata.net/' */
const api = `${host}/api/`
export default {
  namespaced: true,
  state: {
    api: {
      exist: true, // switches the ability to work with api
      host: host,
      admin: `${api}admin/`,
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      }
    },
    blogPage: {},
    simplePage: {},
    category: [
      {
        id: 1,
        title: 'Fréttir',
        url: 'frettir'
      },
      {
        id: 2,
        title: 'Viðburðir',
        url: 'vidburdir'
      },
      {
        id: 3,
        title: 'Reynsluakstur',
        url: 'reynsluakstur'
      }
    ]
  },
  mutations: {
    SET_DATA (state, { field, data }) {
      state[field] = data
    }
  },
  actions,
  getters
}
