import Vue from 'vue'
import Vuex from 'vuex'

// Modules
import blog from './blog/_index'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    blog
  }
})
